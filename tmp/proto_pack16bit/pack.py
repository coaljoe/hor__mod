import PIL
print PIL.PILLOW_VERSION
from PIL import Image
import sys
from glob import glob
from pprint import pprint

d = {}

fn = sys.argv[1]
im = Image.open(fn)
imW, imH = im.size
print im
pix = im.load()
print pix[1,1]



"""
for y in xrange(imH):
	for x in xrange(imW):
		p = pix[x,y]
		upper = p >> 8
		lower = p & 0xFF
		r = upper
		g = lower
		b = 0
		imO_pix[x,y] = (r, g, b)
"""

"""
for y in xrange(0, imH, 1):
	for x in xrange(0, imW, 2):

		def pack_p(p):
			upper = (p >> 8)# & 0xFF
			lower = p & 0xFF
			r = upper
			g = lower
			return (r, g)
		
		v1, v2 = pack_p(pix[x,y])
		v3, v4 = pack_p(pix[x+1, y])
		imO_pix[x/2,y] = (v1,v2,v3,v4)
"""

compact_range = 0xffff/2
compact_val =  16

def compact_float(f):
	#return int(f * compact_range / compact_val) #& 0xFFFF
	return int(f * 0xffff)
	
def expand_to_float(i):
	#return float(i) * compact_val / compact_range
	return float(i) / 0xffff

def encode():
	imO = Image.new('RGB', im.size)
	imO_pix = imO.load()
	for y in xrange(imH):
		for x in xrange(imW):
			p = pix[x,y]
			pf = p / float(0xffff)
			#print(p)
			#exit(1)
			cp = compact_float(pf)
			upper = cp >> 8
			lower = cp & 0xFF
			#print upper, cp
			r = lower
			g = upper
			b = 0
			imO_pix[x,y] = (r, g, b)
	imO.save("out_packed.png")
			
def decode():
	#imO = Image.new('I;16', im.size)
	imO = Image.new('F', im.size)
	imO_pix = imO.load()
	for y in xrange(imH):
		for x in xrange(imW):
			p = pix[x,y]
			#print(p)
			#exit(1)
			r = p[0]
			g = p[1]
			#i = (r + (g >> 8))
			i = ((r >> 8) + g)
			f = expand_to_float(i)
			#f = (g + (r >> 8)) / 1000.0
			imO_pix[x,y] = f
			#imO_pix[x,y] = int(f * 0xffff)
	
	imO.save("out_unpacked.tif")

decode()
#encode()