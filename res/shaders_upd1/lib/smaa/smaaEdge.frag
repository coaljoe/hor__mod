// Edge fragment shader
uniform sampler2D texture;
vec2 texcoord;
vec4 offset[3];
vec4 dummy2;

void main()
{
    //gl_FragColor = SMAAColorEdgeDetectionPS(texcoord, offset, texture);
    gl_FragColor = SMAALumaEdgeDetectionPS(texcoord, offset, texture);
    //gl_FragColor.a = 1.0; // This is not there originally! Instead turn blending off!!
}