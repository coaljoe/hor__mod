#include "lib/fxaa.frag"
uniform sampler2D fboTex;
uniform vec2 ScreenPos;
uniform bool use_fxaa = false;
//uniform bool gamma_correction = true;
uniform bool gamma_correction = false;

void main()
{
    vec3 color;
    color = vec3(0.0,0.0,1.0); // set to blue

    // test
    //vec3 c2 = texture2D(fboTex, ScreenPos).rgb;
    //vec3 c2 = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    //color = (color + c2) / 2;
    //color = vec3(color.r, max(color.g,  color.b), color.b);

	
    //if (true) {
    if (use_fxaa) {
		//derp
        
        /*

        vec2 buffersize = vec2(960, 540);
        //vec2 buffersize = textureSize(fboTex, 0).xy;

        vec2 rcpFrame; 
        rcpFrame.x = 1.0 / buffersize.x;
        rcpFrame.y = 1.0 / buffersize.y;
 
        vec2 pos = gl_FragCoord.xy / buffersize.xy;

        // Only used on FXAA Quality.
        // Choose the amount of sub-pixel aliasing removal.
        // This can effect sharpness.
        //   1.00 - upper limit (softer)
        //   0.75 - default amount of filtering
        //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
        //   0.25 - almost off
        //   0.00 - completely off
        float QualitySubpix = 0.75;
        //float QualitySubpix = 0.3;
        
        // The minimum amount of local contrast required to apply algorithm.
        //   0.333 - too little (faster)
        //   0.250 - low quality
        //   0.166 - default
        //   0.125 - high quality 
        //   0.033 - very high quality (slower)
        //float QualityEdgeThreshold = 0.033;
        float QualityEdgeThreshold = 0.125;
        //float QualityEdgeThreshold = 0.13;

        // skips dark areas from processing (note green luma)
        float QualityEdgeThresholdMin = 0.0625; //0.02;//0.0625; // ?
  
        float dummy1 = 0;
        vec4 dummy4 = vec4(0);
        */
		/*
        color = vec3(FxaaPixelShader(pos, dummy4, fboTex, fboTex, fboTex,
                                     rcpFrame, dummy4, dummy4, dummy4,
                                     QualitySubpix, QualityEdgeThreshold, QualityEdgeThresholdMin,
                                     dummy1, dummy1, dummy1, dummy4));
		*/
        /*
		vec2 r_FBufScale = rcpFrame;
		color = vec3(FxaaPixelShader(
				gl_FragCoord.xy * r_FBufScale, //pos
				vec4(0.0), //not used
				fboTex, //tex
				fboTex, //not used
				fboTex, //not used
				r_FBufScale, //fxaaQualityRcpFrame
				vec4(0.0), //not used
				vec4(0.0), //not used
				vec4(0.0), //not used
				0.75, //fxaaQualitySubpix
				0.166, //fxaaQualityEdgeThreshold
				0.0625, //fxaaQualityEdgeThresholdMin
				0.0, //not used
				0.0, //not used
				0.0, //not used
				vec4(0.0) //not used
			));
        */
        /*
        vec2 r_FBufScale = rcpFrame;
		gl_FragColor = FxaaPixelShader(
				gl_FragCoord.xy * r_FBufScale, //pos
				vec4(0.0), //not used
				fboTex, //tex
				fboTex, //not used
				fboTex, //not used
				r_FBufScale, //fxaaQualityRcpFrame
				vec4(0.0), //not used
				vec4(0.0), //not used
				vec4(0.0), //not used
				0.75, //fxaaQualitySubpix
				0.166, //fxaaQualityEdgeThreshold
				0.0625, //fxaaQualityEdgeThresholdMin
				0.0, //not used
				0.0, //not used
				0.0, //not used
				vec4(0.0) //not used
			);
            */
/*
            vec2 buffersize = vec2(960, 540);
        //vec2 buffersize = textureSize(fboTex, 0).xy;
            vec2 u_ScreenSize = buffersize;
              vec2 rcpFrame = vec2(1.0) / u_ScreenSize.xy; 
    vec2 pos = gl_FragCoord.xy / u_ScreenSize.xy;

  // Only used on FXAA Quality.
    // Choose the amount of sub-pixel aliasing removal.
    // This can effect sharpness.
    //   1.00 - upper limit (softer)
    //   0.75 - default amount of filtering
    //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
    //   0.25 - almost off
    //   0.00 - completely off
    float QualitySubpix = 0.0;

    // The minimum amount of local contrast required to apply algorithm.
    //   0.333 - too little (faster)
    //   0.250 - low quality
    //   0.166 - default
    //   0.125 - high quality 
    //   0.033 - very high quality (slower)
    float QualityEdgeThreshold = 0.033;
    float QualityEdgeThresholdMin = 0.0;

    vec4 ConsolePosPos = vec4(0.0);
    vec4 ConsoleRcpFrameOpt = vec4(0.0);
    vec4 ConsoleRcpFrameOpt2 = vec4(0.0);
    vec4 Console360RcpFrameOpt2 = vec4(0.0);
    float ConsoleEdgeSharpness = 8.0;
    float ConsoleEdgeThreshold = 0.125;
    float ConsoleEdgeThresholdMin = 0.05;
    vec4  Console360ConstDir = vec4(1.0, -1.0, 0.25, -0.25);

    gl_FragColor = FxaaPixelShader(pos, ConsolePosPos, fboTex, fboTex, fboTex, rcpFrame, ConsoleRcpFrameOpt, ConsoleRcpFrameOpt2, Console360RcpFrameOpt2, QualitySubpix, QualityEdgeThreshold, QualityEdgeThresholdMin, ConsoleEdgeSharpness, ConsoleEdgeThreshold, ConsoleEdgeThresholdMin, Console360ConstDir);
*/

	vec2 buffersize = ivec2(960, 540);
	//vec2 buffersize = ivec2(512, 512);
    vec2 pos = gl_FragCoord.xy / buffersize.xy;

	vec4 FragColor;
	FragColor = FxaaPixelShader(
					//ftexcoord,
					pos,
					vec4(0.0),
					fboTex,
					fboTex,
					fboTex,
					1.0/buffersize,
					vec4(0.0),
					vec4(0.0),
					vec4(0.0),
					0.75,
					0.166,
					0.0625,
					0.0,
					0.0,
					0.0,
					vec4(0.0)
					);

	//gl_FragColor = FragColor;
	//gl_FragColor = vec4(FragColor.rgb, 1.0);
	color = FragColor.rgb;

    }
    else {
        color = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    }


    if(gamma_correction)
    {
        //float gamma;
        //gamma = 2.2; // sRGB
        float gamma = 0.8;
        color.rgb = pow(color.rgb, vec3(1.0/gamma));
    }

    
    gl_FragColor.rgb = color;
    //gl_FragColor = vec4(color, 1.0);
    //gl_FragColor = vec4(0.0,0.0,1.0,1.0);
}
