#include "lib/fxaa.frag"
uniform sampler2D fboTex;
uniform vec2 ScreenPos;
uniform bool use_fxaa = true;
uniform bool gamma_correction = true;

void main()
{
    vec3 color;
    //color = vec3(0.0,0.0,1.0);

    // test
    //vec3 c2 = texture2D(fboTex, ScreenPos).rgb;
    //vec3 c2 = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    //color = (color + c2) / 2;
    //color = vec3(color.r, max(color.g,  color.b), color.b);

	
    if (use_fxaa) {
		discard;

        vec2 buffersize = vec2(960, 540);
        //vec2 buffersize = textureSize(fboTex, 0).xy;

        vec2 rcpFrame; 
        rcpFrame.x = 1.0 / buffersize.x;
        rcpFrame.y = 1.0 / buffersize.y;
 
        vec2 pos = gl_FragCoord.xy / buffersize.xy;

        // Only used on FXAA Quality.
        // Choose the amount of sub-pixel aliasing removal.
        // This can effect sharpness.
        //   1.00 - upper limit (softer)
        //   0.75 - default amount of filtering
        //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
        //   0.25 - almost off
        //   0.00 - completely off
        //float QualitySubpix = 0.75;
        float QualitySubpix = 0.3;
        
        // The minimum amount of local contrast required to apply algorithm.
        //   0.333 - too little (faster)
        //   0.250 - low quality
        //   0.166 - default
        //   0.125 - high quality 
        //   0.033 - very high quality (slower)
        //float QualityEdgeThreshold = 0.033;
        //float QualityEdgeThreshold = 0.125;
        float QualityEdgeThreshold = 0.13;

        // skips dark areas from processing (note green luma)
        float QualityEdgeThresholdMin = 0.0625; //0.02;//0.0625; // ?
  
        float dummy1 = 0;
        vec4 dummy4 = vec4(0);
        color = vec3(FxaaPixelShader(pos, dummy4, fboTex, fboTex, fboTex,
                                     rcpFrame, dummy4, dummy4, dummy4,
                                     QualitySubpix, QualityEdgeThreshold, QualityEdgeThresholdMin,
                                     dummy1, dummy1, dummy1, dummy4));

    }
    else {
        color = texture2D(fboTex, gl_TexCoord[0].st).rgb;
    }


    if(gamma_correction)
    {
        //float gamma;
        //gamma = 2.2; // sRGB
        float gamma = 0.8;
        color.rgb = pow(color.rgb, vec3(1.0/gamma));
    }

    
    //gl_FragColor.rgb = color;
}

/* vim:set ft=glsl: */
