uniform sampler2D fboTex;
uniform sampler2D depthTex;
uniform sampler2D bgTex;
uniform sampler2D bgDepthTex;
uniform sampler2D l1ObjTex;
uniform sampler2D l1ObjDepthTex;
uniform sampler2D l3OuterWallsTex0;
uniform sampler2D l3OuterWallsDepthTex0;
uniform sampler2D l3OuterWallsTex1;
uniform sampler2D l3OuterWallsDepthTex1;
uniform sampler2D l3OuterWallsTex2;
uniform sampler2D l3OuterWallsDepthTex2;
uniform sampler2D cutawayMaskTex;
uniform sampler2D cutawayMaskDepthTex;
uniform sampler2D l2InnerWallsTex0;
uniform sampler2D l2InnerWallsDepthTex0;
uniform vec2 ScreenPos;

float LinearizeDepth(float zoverw)
{
    float n = 1.0;
    float f = 40.0;
	
    return(2.0 * n) / (f + n - zoverw * (f - n));	
}

float LinearizeDepth2(float zoverw)
{
    float n = 1.0;
    float f = 100.0;
	
    //return (zoverw - 1.0) * (f - n);
    //return n + zoverw*(f - n);
    return(2.0 * n) / (n + zoverw * (f - n));	
}

/*
float LinearizeDepth3(float z_b)
{
    float zNear = 1.0;
    float zFar = 100.0;
	
    float z_n = 2.0 * z_b - 1.0;
    float z_e = 2.0 * zNear * zFar / (zFar + zNear - z_n * (zFar - zNear));

	return z_e;
}
*/
float LinearizeDepth4(float z)
{
    float n = 1.0;
    float f = 100.0;
    /*float f = 100.0;*/

	//return n * (z + 1.0) / (f + n - z * (f - n));
	return (2.0 * n) / (f + n - z * (f - n));
	//return n * (z + 1.0) / (f + n - z * (f - n));
    /*float z_n = 2.0 * z - 1.0;*/
	//return (z_n - n) / f;
	/*return (z_n - n) * f;*/
    /*float z_e = 2.0 * n * f / (f + n - z_n * (f - n));*/
	/*return z_e;*/
}

void main()
{
    vec4 r;

	vec2 tc = gl_TexCoord[0].st;

    // FboTex
    vec4 color = texture2D(fboTex, gl_TexCoord[0].st);

    // DepthTex
    float depth = texture2D(depthTex, gl_TexCoord[0].st).r;
    //depth = LinearizeDepth(depth)*77;
    //depth = LinearizeDepth(depth)*6;

    // BgTex
    vec4 bgColor = texture2D(bgTex, gl_TexCoord[0].st);

    // BgDepthTex
    float bgDepth = texture2D(bgDepthTex, gl_TexCoord[0].st).r;

    // l1 tex
    vec4 l1ObjColor = texture2D(l1ObjTex, tc);

    // l1 depth
    float l1ObjDepth = texture2D(l1ObjDepthTex, tc).r;

    // Outer Walls
    // l3 tex0
    vec4 l3OuterWalls0Color = texture2D(l3OuterWallsTex0, tc);

    // l3 depth0
    float l3OuterWalls0Depth = texture2D(l3OuterWallsDepthTex0, tc).r;

    // l3 tex1
    vec4 l3OuterWalls1Color = texture2D(l3OuterWallsTex1, tc);

    // l3 depth1
    float l3OuterWalls1Depth = texture2D(l3OuterWallsDepthTex1, tc).r;

    // l3 tex2
    vec4 l3OuterWalls2Color = texture2D(l3OuterWallsTex2, tc);

    // l3 depth2
    float l3OuterWalls2Depth = texture2D(l3OuterWallsDepthTex2, tc).r;

    // l2 tex0
    vec4 l2InnerWalls0Color = texture2D(l2InnerWallsTex0, tc);

    // l2 depth0
    float l2InnerWalls0Depth = texture2D(l2InnerWallsDepthTex0, tc).r;


    // Cutaway
    //vec4 cutawayMaskColor = color;
    vec4 cutawayMaskColor = texture2D(cutawayMaskTex, tc);

    //float cutawayMaskDepth = depth;
    float cutawayMaskDepth = texture2D(cutawayMaskDepthTex, tc).r;


    /* Mix dynamic objects */
    //if (depth < bgDepth) {
    //if (depth < min(l1ObjDepth, bgDepth)) {
    //if (depth < l1ObjDepth || depth < bgDepth) {
    //if (depth < l1ObjDepth) {
    //if (depth < min(l1ObjDepth, l3OuterWalls0Depth)) {
    if ((depth < l1ObjDepth) || (depth < l3OuterWalls0Depth)) {
    //if ((depth < l1ObjDepth) && (depth < l3OuterWalls0Depth)) {
        r = color;

        /*
        if (color.r > 0.7) {
            r = l3OuterWalls0Color;
        }
        */
        
        if (l3OuterWalls0Depth < depth) {
            if (!(cutawayMaskColor.r > 0)) {
                r = l3OuterWalls0Color;
                //r = vec4(1.0, 0, 0, 1.0);
            }
        }

    } else {
        /* Mix non-dynamic layers */

         //if (l2InnerWalls0Depth > l1ObjDepth) {
        if (l1ObjDepth < l2InnerWalls0Depth) {
            r = l1ObjColor;
        } else {
            //r = bgColor;
            //r = vec4(1.0, 0, 0, 1.0);
            r = l2InnerWalls0Color;
        }

        /*
        //r = bgColor;
		//if (bgDepth >= l1ObjDepth) {
        if (l3OuterWalls0Depth > l1ObjDepth) {
			r = l1ObjColor;
		} else {
			//r = bgColor;
            r = l3OuterWalls0Color;
		}
		//r = l1ObjColor;
        */

        /*
        if (cutawayMaskColor.r > 0.7) {
            r = l1ObjColor;
        }
        */

        // Outer wall
        //if (l3OuterWalls0Color.a > 0) {
        //if (l3OuterWalls0Depth < l2InnerWalls0Depth) {
        if (l3OuterWalls0Depth < l2InnerWalls0Depth && l3OuterWalls0Depth < l1ObjDepth) {
            r = vec4(1.0, 1.0, 1.0, 1.0);
            r = l3OuterWalls0Color;
        }

        //if (cutawayMaskColor.r > 0.7) {
        //if (cutawayMaskColor.r != 0.0) {
        if (cutawayMaskColor.r > 0) {
            //r = l1ObjColor;
            //r = (l3OuterWalls0Color * 0.2) + (l1ObjColor * 0.8);
            //if (l3OuterWalls0Depth < 0) {

            // Fill it with background color
            r = l1ObjColor;
            //r = vec4(1.0, 0, 0, 1.0);

            if (l2InnerWalls0Color.a > 0) {
                //r = vec4(1.0, 0, 0, 1.0);
                //r = mix(r, l2InnerWalls0Color, 0.5);
                //r = mix(r, l2InnerWalls0Color, 0.2);
                //r = l2InnerWalls0Color;
                
                //if (l1ObjDepth > l2InnerWalls0Depth) {
                if (l3OuterWalls0Depth > l2InnerWalls0Depth) {
                    //r = l1ObjColor;
                    //r = vec4(0.0, 1, 0, 1.0);
                    r = mix(r, l2InnerWalls0Color, 0.2);
                } else {
                    //r = vec4(1.0, 0, 0, 1.0);
                    r = mix(r, l3OuterWalls0Color, 0.2);
                }
                

            }

            //if (l3OuterWalls0Color.a > 0) {
            if (l3OuterWalls0Depth < cutawayMaskDepth) {
                r = vec4(1.0, 0, 0, 1.0);
                // Find other outer walls depths at this point

                if (l3OuterWalls0Depth < l3OuterWalls1Depth) {
                    r = vec4(0, 1, 0, 1);
                    //r = l3OuterWalls0Color;
                    //r = l1ObjColor;
                    r = mix(l3OuterWalls0Color*50, l1ObjColor*0.8, 0.999);
                }

                if (l3OuterWalls1Depth < l3OuterWalls2Depth) {
                    //r = vec4(0, 0, 1, 1);
                    //r = l3OuterWalls1Color;
                    //r = mix(l3OuterWalls0Color*50, l3OuterWalls1Color*0.8, 0.999);
                    /*
                    if (l3OuterWalls1Depth < cutawayMaskDepth) {
                        r = vec4(1, 0, 1, 1);
                    }
                    */

                    // Only drawing things behind cutaway mask (removes small triangles)
                    if (!(l3OuterWalls1Depth < cutawayMaskDepth)) {
                        //r = vec4(1, 0, 1, 1);
                        r = mix(l3OuterWalls0Color*50, l3OuterWalls1Color*0.8, 0.999);
                    }
                }

                // Inner wall
                //if (l3OuterWalls0Depth > l2InnerWalls0Depth) {
                if (l2InnerWalls0Depth < 1) {
                    //r = vec4(0, 0, 1, 0);
                    r = l2InnerWalls0Color;

                    if (l3OuterWalls0Depth < l2InnerWalls0Depth) {
                        r = vec4(0, 0, 1, 0);
                    }

                    if (l1ObjDepth < l2InnerWalls0Depth) {
                        r = vec4(1, 0, 1, 0);
                    }
                }
            }
            
            if (l3OuterWalls0Color.a > 0) {
                //r = mix(l3OuterWalls0Color, l1ObjColor, 0.9);
                //r = mix(l3OuterWalls0Color, l1ObjColor, 0.4);
                
                /*
                //if (l1ObjDepth > l3OuterWalls1Depth) {
                //if (true) {
                if (l3OuterWalls0Depth > l3OuterWalls1Depth) {
                    r = vec4(0.0, 1.0, 0.0, 1.0);
                    //r = l3OuterWalls1Color;
                }
                else if (l1ObjDepth > l3OuterWalls0Depth) {
                    r = vec4(1.0, 0, 0, 1.0);
                    //r = l3OuterWalls0Color;
                    //r = l1ObjColor;
                }
                */

                //r = vec4(0, 0, 1.0, 1.0);
                //r = l3OuterWalls0Color;

                //if (l1ObjDepth > l3OuterWalls0Depth) {
                //if (false) {
                if (l1ObjDepth < l3OuterWalls0Depth) {
                    //r = vec4(1.0, 0, 0, 1.0);
                    //r = l3OuterWalls0Color;
                    //r = l1ObjColor;
                }

                /*
                bool test = false;
                bool test2 = false;
                bool test1 = false;

                //if (l3OuterWalls1Color.a > 0 && l3OuterWalls1Depth > l3OuterWalls0Depth) {
                if (l3OuterWalls1Color.a > 0 && (l3OuterWalls1Depth > l3OuterWalls0Depth)) {
                //if (l3OuterWalls1Depth > 0xffff && (l3OuterWalls1Depth > l3OuterWalls0Depth)) {
                    //r = vec4(0.0, 1 , 0, 1.0);
                    r = l3OuterWalls1Color;
                    //r = l1ObjColor;
                    test = true;
                    test2 = true;
                }

                if (l1ObjColor.a > 0 && (l1ObjDepth > l3OuterWalls0Depth)) {
                    r = vec4(1.0, 0, 1.0, 1.0);
                    //r = l3OuterWalls0Color;
                    r = l1ObjColor;
                    test = true;
                }
                */

                // Finally redraw overlapped dynamic
                /*
                if (color.a > 0) {
                    r = vec4(1.0, 0, 1.0, 1.0);
                }
                */

                // XXX: FIXME: record and test cutaway depth
                // XXX: should not be used when player is outside of cutaway volume (cutaway object is off then)
                //if (color.a > 0 && test && (cutawayMaskDepth > l3OuterWalls0Depth)) {
                //if (color.a > 0 && test && (depth < l3OuterWalls1Depth)) {
                /*
                if (color.a > 0 && test) {
                    r = vec4(1.0, 0.5, 1.0, 1.0);
                    if (test2 && (depth < l3OuterWalls1Depth)) {
                        r = vec4(1.0, 0, 1.0, 1.0);
                    }
                    if (!test2) {
                    //if (true) {
                        r = vec4(0, 0, 1, 1);
                    }
                }
                */
                
                

            } /*else {
                r = l1ObjColor;
                r = vec4(1.0, 0, 0, 1.0);
            }
            */            
            
        }

        


        //r = bgColor + color.r * 0.1;
        //r = bgColor + (((color.r + color.g + color.b)/3) * 0.05);
        //r = bgColor + 0.05;
        /*
        vec4 v = vec4(0, 0, 0, 0);
        if(max(color,v) == color) {
            float sw = 960;
            float sh = 540;
            if(int(gl_TexCoord[0].y * sh) % 2 == 0 && 
               int(gl_TexCoord[0].x * sw) % 2 != 0) {
                float avg = ((bgColor.r + bgColor.g + bgColor.b)/3.0);
                r = vec4(avg, avg, avg, 1.0);
                //r = color;
             }
         }
         */
    }



    /*
    if (gl_TexCoord[0].x < 0.5) {
        r = bgColor;
    } else {
        r = color;
    }
    */

    // gamma corrected
    // #r = r * (1.0/2.2); // wrong
    /*
    float g = 1.0/2.2;
    r = pow(r, vec4(g, g, g, 1.0));
    */

    //r = l3OuterWalls2Color;
    //r = l2InnerWalls0Color;

    //r = vec4(bgDepth * 65535.0, 0.0, 0.0, 1.0);
    //r = vec4(bgDepth * 255, bgDepth * 255, bgDepth * 255, 1.0);
    //r = vec4(bgDepth, 0, 0, 1.0);
    gl_FragColor = vec4(r.r, r.g, r.b, 1.0);
}

/* vim:set ft=glsl: */
