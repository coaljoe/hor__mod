#!/usr/bin/env python2
# combines tile images to one big image
import sys
from PIL import Image

if len(sys.argv) < 2:
    print "usage: comb.py sx sy [prefix]"
    exit(1)

size = 512
#prefix = "depth"
prefix = "r"
try: prefix = sys.argv[3]
except IndexError: pass
sx = int(sys.argv[1])
sy = int(sys.argv[2])


im_h = sx*size
im_w = sy*size
im = Image.new("RGBA", ((sx+(sy-1))*size, sy*size))

for y in range(sy):
    for x in range(sx):
        file = "%s_%03d_%03d.png" % (prefix, x, y)
        im1 = Image.open(file)
        px = (((sx-1)*size) + (x*size)) - y*size
        py = im.height - ((y+1)*size)
        print (px, py, im.height)
        im.paste(im1, (px, py))

outfn = "out_%s.tga" % prefix
im.save(outfn, quality=0)
print "saved", outfn
print "done"
