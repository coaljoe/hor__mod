#!/bin/bash
dir=$1
if [ ! -d $dir ]; then
    echo 'error: output must be a directory'
    exit -1
fi

#nohup ~/util/boost.sh 20 2>&1 >/dev/null &
cd $dir
echo $PWD

##find -iname \*.png -print -exec pngcrush -reduce {} {} \;
#find -name \*.PNG -print -exec pngcrush -brute -reduce {} {} \;
# lossy
find -iname \*.png ! -iname \*depth\* -print -exec pngquant -v --speed 1 --force --skip-if-larger -o {} {} \;
find -iname \*depth\*.png -print -exec optipng {} \;


jpeg_recompress=/home/k/build/jpeg-archive/jpeg-recompress
#find -iname \*.jp?g -exec echo jpeg-recompress {} \;
#find -name \*.jpg -print -exec ../jpeg-recompress {} {} \;
find \( -iname \*.jpg -or -iname \*.jpeg \) -print -exec $jpeg_recompress --quality low {} {} \;

echo "finished"
