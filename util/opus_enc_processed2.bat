set opusenc=C:\Users\k\k\progs\opus-tools-0.1.9-win32\opusenc
set ffmpeg=C:\Users\k\k\progs\ffmpeg-20160901-be07c25-win64-static\bin\ffmpeg
setlocal enableDelayedExpansion
rem SETLOCAL EnableExtensions DisableDelayedExpansion

chcp 65001
set "x=%~n1.opus_meta.txt"
rem %ffmpeg% -i "%~1" -f wav -acodec pcm_s16le -ac 2 -af "crystalizer=i=5, bs2b=profile=jmeier, agate, pan=stereo|FL < 0.6*FL + 0.4*FR | FR < 0.6*FR + 0.4*FL" -nostdin - | %opusenc% --raw --raw-rate 44100 --discard-pictures --discard-comments --comment COMMENT="See \"!x!\" for more details" - "%~dpn1_processed.opus"
rem %ffmpeg% -i "%~1" -f wav -acodec pcm_s16le -ac 2 -af "stereotools=mode=lr>ms, pan=stereo|FL < 0.6*FL + 0.4*FR | FR < 0.6*FR + 0.4*FL" -nostdin - | %opusenc% --raw --raw-rate 44100 --discard-pictures --discard-comments --comment COMMENT="See \"!x!\" for more details" - "%~dpn1.opus"
%ffmpeg% -i "%~1" -f wav -acodec pcm_s16le -ac 2 -af "pan=stereo|FL < 0.7*FL + 0.3*FR | FR < 0.7*FR + 0.3*FL" -nostdin - | %opusenc% --raw --raw-rate 44100 --discard-pictures --discard-comments --comment COMMENT="See \"!x!\" for more details" - "%~dpn1.opus"
rem | %opusenc% -discard-pictures --discard-comments --comment COMMENT="See \"!x!\" for more details" - "%~dpn1.opus"
rem %ffmpeg% -i "%~1" -vn - | %opusenc% --raw --raw-sampling 44100 -discard-pictures --discard-comments --comment COMMENT="See \"!x!\" for more details" - "%~dpn1.opus"
rem %opusenc% --discard-pictures --discard-comments --comment COMMENT="See \"!x!\" for more details" "%~1" "%~dpn1.opus"
rem %opusenc% --discard-pictures --discard-comments "%~1" "%~dpn1.opus"
pause