package main

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/coaljoe/hor__mod/src"
)

// entry point
func main() {
	fmt.Println("main()")
	/*
		// doesn't work with .exe linking
		if runtime.GOOS == "windows" {
			envPath := os.Getenv("PATH")
			nEnvPath := envPath + string(os.PathListSeparator) + _path.Join("lib", "dll")
			os.Setenv("PATH", nEnvPath)
			fmt.Println(os.Environ())
			fmt.Println(nEnvPath, " ", os.Getenv("PATH"))
			panic(2)
		}
	*/
	// cd to app's root
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	fmt.Println("dir:", dir)
	//appRoot := filepath.Join(dir, "..")
	appRoot := dir
	fmt.Println("appRoot: ", appRoot)
	os.Chdir(appRoot)
	//panic(2)

	src.Init()
	src.Main()
}
