module gitlab.com/coaljoe/hor__mod

require (
	bitbucket.org/coaljoe/lib v0.0.0-20190306224124-ae61fdea8bb6
	bitbucket.org/coaljoe/rx v0.0.0-20190212221502-b4bef7633f19
	github.com/bitly/go-simplejson v0.5.0
	github.com/davecgh/go-spew v1.1.1
	github.com/go-gl/gl v0.0.0-20181026044259-55b76b7df9d2
	github.com/go-gl/glfw v0.0.0-20190217072633-93b30450e032
	github.com/stretchr/testify v1.3.0 // indirect
)

replace bitbucket.org/coaljoe/rx => ../rx_orig
