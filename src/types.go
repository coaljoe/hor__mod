package src

type UpdateableI interface {
	update(dt float64)
}
