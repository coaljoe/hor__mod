package src

import (
	"bitbucket.org/coaljoe/rx"
	"github.com/go-gl/gl/v2.1/gl"
)

type TestDepthCombRenderPass struct {
	*rx.RenderPass
	BgDepthTex            *rx.Texture
	BgTex                 *rx.Texture
	l1ObjTex              *rx.Texture
	l1ObjDepthTex         *rx.Texture
	l3OuterWallsTex0      *rx.Texture
	l3OuterWallsDepthTex0 *rx.Texture
	l3OuterWallsTex1      *rx.Texture
	l3OuterWallsDepthTex1 *rx.Texture
	l3OuterWallsTex2      *rx.Texture
	l3OuterWallsDepthTex2 *rx.Texture
	l2InnerWallsTex0      *rx.Texture
	l2InnerWallsDepthTex0 *rx.Texture
	// fixme?
	combShader         *rx.Shader
	fboRt              *rx.RenderTarget
	depthRt            *rx.RenderTarget
	cutawayMaskRt      *rx.RenderTarget
	cutawayMaskDepthRt *rx.RenderTarget
	cutawayMaskNode    *rx.Node
}

func NewTestDepthCombRenderPass() (rp *TestDepthCombRenderPass) {
	rp = &TestDepthCombRenderPass{
		RenderPass: rx.NewRenderPass("NewTestTestDepthCombRenderPass"),
	}
	return
}

func (rp *TestDepthCombRenderPass) Init(r *rx.Renderer) {
	width, height := r.Size()
	// fixme?
	rp.combShader = rx.NewShader("shaders/test_camera_fsquad_depth_combine.vert",
		"shaders/test_camera_fsquad_depth_combine.frag")
	rp.depthRt = rx.NewRenderTarget("depthRt", width, height, true)
	rp.fboRt = rx.NewRenderTarget("fboRt", width, height, false)
	rp.cutawayMaskRt = rx.NewRenderTarget("cutawayMaskRt", width, height, false)
	rp.cutawayMaskDepthRt = rx.NewRenderTarget("cutawayMaskDepthRt", width, height, true)
}

func (rp *TestDepthCombRenderPass) Render(r *rx.Renderer) {

	//cam := r.curCam

	// Render dynamic objects to fbo
	//
	rp.fboRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	r.RenderAll()

	//rp.depthRt.Unbind()
	rp.fboRt.Unbind()

	// Render scene to depthRt.
	//
	rp.depthRt.Bind()

	// clear tmp_rt texture
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	// render scene
	//r.renderScene()
	//r.renderRenderables()
	//r.renderDebug()
	r.RenderAll()

	rp.depthRt.Unbind()

	// Render cutawayMaskRt

	rp.cutawayMaskRt.Bind()
	toggleBack := false
	if !rp.cutawayMaskNode.Visible() {
		rp.cutawayMaskNode.ToggleVisible()
		toggleBack = true
	}
	//gl.Enable(gl.CULL_FACE)
	//gl.CullFace(gl.FRONT)
	r.Clear()
	//rp.cutawayMaskNode.Render()
	r.SetActiveNode(rp.cutawayMaskNode)
	r.RenderAll()
	rp.cutawayMaskRt.Unbind()
	r.SetActiveNode(nil)
	gl.CullFace(gl.BACK)
	//gl.Disable(gl.CULL_FACE)
	if toggleBack {
		rp.cutawayMaskNode.ToggleVisible()
	}

	// Render cutawayMaskDepthRt

	rp.cutawayMaskDepthRt.Bind()
	toggleBack = false
	if !rp.cutawayMaskNode.Visible() {
		rp.cutawayMaskNode.ToggleVisible()
		toggleBack = true
	}
	r.Clear()
	//rp.cutawayMaskNode.Render()
	r.SetActiveNode(rp.cutawayMaskNode)
	r.RenderAll()
	rp.cutawayMaskDepthRt.Unbind()
	r.SetActiveNode(nil)
	if toggleBack {
		rp.cutawayMaskNode.ToggleVisible()
	}

	// Render the cam.rt texture with fsquad shader.
	//
	rp.combShader.Bind()
	rp.combShader.PassUniform1i("fboTex", 0)
	rp.combShader.PassUniform1i("depthTex", 1)
	rp.combShader.PassUniform1i("bgTex", 2)
	rp.combShader.PassUniform1i("bgDepthTex", 3)
	rp.combShader.PassUniform1i("l1ObjTex", 4)
	rp.combShader.PassUniform1i("l1ObjDepthTex", 5)
	// Outer walls
	rp.combShader.PassUniform1i("l3OuterWallsTex0", 6)
	rp.combShader.PassUniform1i("l3OuterWallsDepthTex0", 7)
	rp.combShader.PassUniform1i("l3OuterWallsTex1", 8)
	rp.combShader.PassUniform1i("l3OuterWallsDepthTex1", 9)
	rp.combShader.PassUniform1i("l3OuterWallsTex2", 10)
	rp.combShader.PassUniform1i("l3OuterWallsDepthTex2", 11)
	// Cutaway mask
	rp.combShader.PassUniform1i("cutawayMaskTex", 12)
	rp.combShader.PassUniform1i("cutawayMaskDepthTex", 13)
	// Inner walls
	rp.combShader.PassUniform1i("l2InnerWallsTex0", 14)
	rp.combShader.PassUniform1i("l2InnerWallsDepthTex0", 15)

	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, rp.fboRt.Tex())
	gl.ActiveTexture(gl.TEXTURE1)
	gl.BindTexture(gl.TEXTURE_2D, rp.depthRt.Tex())
	gl.ActiveTexture(gl.TEXTURE2)
	gl.BindTexture(gl.TEXTURE_2D, rp.BgTex.Tex())
	gl.ActiveTexture(gl.TEXTURE3)
	gl.BindTexture(gl.TEXTURE_2D, rp.BgDepthTex.Tex())
	gl.ActiveTexture(gl.TEXTURE4)
	gl.BindTexture(gl.TEXTURE_2D, rp.l1ObjTex.Tex())
	gl.ActiveTexture(gl.TEXTURE5)
	gl.BindTexture(gl.TEXTURE_2D, rp.l1ObjDepthTex.Tex())
	// Outer walls
	gl.ActiveTexture(gl.TEXTURE6)
	gl.BindTexture(gl.TEXTURE_2D, rp.l3OuterWallsTex0.Tex())
	gl.ActiveTexture(gl.TEXTURE7)
	gl.BindTexture(gl.TEXTURE_2D, rp.l3OuterWallsDepthTex0.Tex())
	gl.ActiveTexture(gl.TEXTURE8)
	gl.BindTexture(gl.TEXTURE_2D, rp.l3OuterWallsTex1.Tex())
	gl.ActiveTexture(gl.TEXTURE9)
	gl.BindTexture(gl.TEXTURE_2D, rp.l3OuterWallsDepthTex1.Tex())
	gl.ActiveTexture(gl.TEXTURE10)
	gl.BindTexture(gl.TEXTURE_2D, rp.l3OuterWallsTex2.Tex())
	gl.ActiveTexture(gl.TEXTURE11)
	gl.BindTexture(gl.TEXTURE_2D, rp.l3OuterWallsDepthTex2.Tex())
	// Cutaway mask
	gl.ActiveTexture(gl.TEXTURE12)
	gl.BindTexture(gl.TEXTURE_2D, rp.cutawayMaskRt.Tex())
	gl.ActiveTexture(gl.TEXTURE13)
	gl.BindTexture(gl.TEXTURE_2D, rp.cutawayMaskDepthRt.Tex())
	// Inner walls
	gl.ActiveTexture(gl.TEXTURE14)
	gl.BindTexture(gl.TEXTURE_2D, rp.l2InnerWallsTex0.Tex())
	gl.ActiveTexture(gl.TEXTURE15)
	gl.BindTexture(gl.TEXTURE_2D, rp.l2InnerWallsDepthTex0.Tex())

	r.RenderFsQuad()

	rp.combShader.Unbind()
	gl.BindTexture(gl.TEXTURE_2D, 0)

	// end
}
