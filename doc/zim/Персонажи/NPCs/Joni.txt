Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2014-02-11T17:38:52+04:00

====== Joni ======

Yoni, the Drunkard

=== Character information ===

* **Name**: Joni 
* **Age**: 
* **Religion**: (optional) 
* **Languages**: 
* **Mental**: (Below / Average / Above) 
* **Intelligence**: 
* **Perception**: 
* **Social**: (Below / Average / Above) 
* **Charisma**: 
* **Manipulation**: 
* **Physical**: (Below / Average / Above) 
* **Combatant**: (Aggressive / Only when Provoked / Non-Violent)

=== Physical Appearance ===
* **General height, weight, and apparent age: **
* **Unique features: **
* **Usual attire: **

=== Background information ===

== History ==
* **What is the NPC's history? Be as detailed as possible. **
* **What is this NPC trying to accomplish?** 

== Ties to other NPCs or Political groups ==
* **Does this NPC have ties to other NPCs or Political groups? Descriptions of ties with wiki links.** 

== Relationship to the PC ==
* **Does this NPC have ties to the PC? If not, how would the PC meet them? **
* **How can the relationship evolve? **
* **What are the differences for a male / female PC?**

=== Quest options ===
* **What quests could the player receive from this NPC?**
